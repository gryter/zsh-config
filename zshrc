# Path to your oh-my-zsh configuration.
ZSH=$HOME/.oh-my-zsh

#
# Theme
#

# Set name of the theme to load. 
# Look in ~/.oh-my-zsh/themes/
ZSH_THEME="garrys_theme"

# Configure the theme with style
# Limit the number of dir shown to 2
zstyle :prompt:garrys path subdir
zstyle :prompt:garrys subdir 2

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Example format: plugins=(rails git textmate ruby lighthouse)
plugins=(git brew)

# Disable autosetting terminal title
DISABLE_AUTO_TITLE="true"

# Launch oh-my-zsh
source $ZSH/oh-my-zsh.sh

# Load all aliases from separate file
source ~/zsh/aliases

#
# Variables
#

export EDITOR="nano"

# case-insensitive search in less and don’t clear on exit
# WARNING: moved to .zshrc because .oh-my-zsh/lib/misc.zsh overrides this one. bad oh-my-zsh
export LESS="-Ri --no-init"

export PATH=/usr/local/php5/bin:~/zsh/bin:$PATH

#
# Options
#

# cd command is there for a reason
unsetopt AUTO_CD

# Disable autocomplete menu and reorganisation
setopt NO_AUTO_MENU
setopt REC_EXACT

# Do not exit on end-of-file
setopt IGNORE_EOF

# Disable spell check on commands and arguments (because sometimes it's annoying)
unsetopt CORRECT
unsetopt CORRECT_ALL

# This will use named dirs when possible
setopt AUTO_NAME_DIRS

# Disable flow control (Ctrl-s & Ctrl-q) to stop/resume output to the terminal
setopt NO_FLOW_CONTROL

# Beeps are annoying
setopt NO_BEEP
