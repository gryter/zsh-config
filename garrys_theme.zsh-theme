# From: http://zshwiki.org/home/config/prompt

prompt_garrys_help ()
{
  cat <<'EOF'
This prompt is configurable via styles:

  Context: :prompt:garrys

  Colors (in zsh/terminfo value):
    host-color - the color for host. defaults to 'blue'
    user-color - the color for user. defaults to 'green'
    root-color - the color for the hostname for root. defaults to 'red'
    path-color - the color for the path. defaults to 'cyan'

  Path type:
    path - possible values:
      ratio - use COLUMNS/ratio to clip the path. Default.
      fixed - use a fixed maximum lenght.
      subdir - clip by number of subdirectories.
      full - show the full path

  Path lenght styles:
    ratio - the ratio for the 'ratio' path style, funnily enough.
            default to 6.
    length - the maximin lenght for the 'fixed' path style.
             defaults to 20
    subdir - the number of subdirs to show for the 'subdir' path style.
             defaults to 3.

  You can set styles in the current terminal to test things out, values
  will be updated.

EOF
}

prompt_garrys_setup ()
{
	setopt noxtrace localoptions

	precmd ()
	{
		local p_full
		local p_tchars p_temp p_done p_last i
		local maxlength ratio
		local host_color
		local user_color
		local root_color
		local path_color
		local path_style
		
		# Color variables and defaults
		zstyle -s :prompt:garrys user-color user_color
		user_color=${user_color:-'green'}
		zstyle -s :prompt:garrys host-color host_color
		host_color=${host_color:-'blue'}
		zstyle -s :prompt:garrys root-color root_color
		root_color=${root_color:-'red'}
		zstyle -s :prompt:garrys path-color path_color
		path_color=${path_color:-'cyan'}
		
		[[ -z $(functions zsh/terminfo) ]] && autoload -Uz zsh/terminfo
		
		# Prepare variables for host and user with color if available
		if [[ "$terminfo[colors]" -ge 8 ]]; then
			if [[ "$EUID" = "0" ]] || [[ "$USER" = 'root' ]]
			then
				user_prompt="%{$fg_bold[$root_color]%}%n%{$reset_color%}"
			else
				user_prompt="%{$fg_bold[$user_color]%}%n%{$reset_color%}"
			fi
			host_prompt="%{$fg_bold[$host_color]%}%m%{$reset_color%}"
			post_prompt="%{$reset_color%}"
		else
			host_prompt="%m"
			user_prompt="%n"
			post_prompt=""
		fi

		if zstyle -t :prompt:garrys path full ratio fixed subdir; then
			zstyle -s :prompt:garrys path path_style
		else
			path_style='ratio'
		fi

		case "${path_style}" in
			ratio)
			zstyle -s :prompt:garrys ratio ratio
			ratio=${ratio:-6}
			maxlength=$(( ${COLUMNS} / ${ratio} ))
			;;
			fixed)
			zstyle -s :prompt:garrys length maxlength
			maxlength=${maxlength:-20}
			;;
			subdir)
			zstyle -s :prompt:garrys subdir maxlength
			maxlength=${maxlength:-3}
			;;
		esac

		case "${path_style}" in
			full)
			path_prompt=$(print -P %~)
			;;
			subdir)
			path_prompt=$(print -P "%($(( ${maxlength} + 1 ))~|..|)%${maxlength}~")
			;;
			ratio|fixed)
			path_prompt=$(print -P %~)
			if (( ${#path_prompt} > ${maxlength} )); then
				p_tchars='../'
				p_done=${path_prompt}
				for (( i=1 ; ; ++i )); do
				p_temp=$(print -P %${i}~)
				if (( ( ${#p_temp} + ${#p_tchars} ) < ${maxlength} )); then
					p_done=${p_temp}
				else
					break
				fi
				done
				path_prompt=${p_tchars}${p_done}
			fi
			;;
		esac
		
		if [[ "$terminfo[colors]" -ge 8 ]]; then
			path_prompt="%{$fg_bold[$path_color]%}${path_prompt}%{$reset_color%}"
		fi

		PS1="[$host_prompt:$path_prompt] $user_prompt> $post_prompt"
		PS2="[$host_prompt:$path_prompt] $user_prompt%_> $post_prompt"
		PS3="[$host_prompt:$path_prompt] $user_prompt?# $post_prompt"
		RPROMPT="$(git_prompt_info)$post_prompt"
	}

	preexec () { }
}

# Print the prompt
prompt_garrys_setup

function git_prompt_info()
{
	echo "$(parse_git_dirty)$ZSH_THEME_GIT_PROMPT_PREFIX$(current_branch)$ZSH_THEME_GIT_PROMPT_SUFFIX"
}

ZSH_THEME_GIT_PROMPT_PREFIX=""
ZSH_THEME_GIT_PROMPT_DIRTY="%{$fg_bold[red]%}"
ZSH_THEME_GIT_PROMPT_CLEAN="%{$fg_bold[green]%}"
ZSH_THEME_GIT_PROMPT_SUFFIX="%{$reset_color%}"
